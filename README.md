#### INSTALL
* git clone https://gitlab.com/litt-arts-num/omeka-docker.git
* cd omeka-docker
* cp .env.dist .env
* cp db.ini.dist db.ini
* docker-compose up -d
* docker-compose exec omeka bash
  > chmod -R 777 /var/www/html/files /var/www/html/plugins /var/www/html/themes; exit
* personnaliser le cas échéant config.ini et relancer le docker
  > sudo docker-compose stop
  > sudo docker-compose up -d --build
* Go to URL:8001/install/install.php

#### THEME & PLUGINS
* Aller sur https://omeka.org/classic/plugins/ et copier l'url de téléchargement direct du plugin ($url)
* Aller sur le serveur (en ssh), dans le dossier du projet
* Aller dans le sous-dossier dédié aux extensions (cd plugins/) et récupérer le zip du plugin : curl -LO $url
* Dé-zipper et supprimer le zip : unzip $zipfile ; rm $zipfile
* Tout le reste se déroule directement sur le site lui-même (activation et paramétrage du plugin)
* Rem. Idem pour un thème sauf que le sous-dossier dédié est themes/

### NOTES
* config.ini a été configuré pour utiliser le smtp de notre hébergeur
